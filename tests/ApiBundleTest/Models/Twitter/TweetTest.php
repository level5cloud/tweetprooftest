<?php
namespace ApiBundleTest\Models;

use ApiBundle\Models\Twitter\Tweet;

class TweetTest extends \PHPUnit_Framework_TestCase
{
    public function testTweetModel()
    {
        $tweetArray = [
            'created_at' => '2016-01-20',
            'id' => 133,
            'retweeted_status' => '123',
            'user' => [
                'id' => 555,
                'name' => 'Test User',
                'friends_count' => 100,
                'followers_count' => 222,
                'favourites_count' => 333,
            ],
        ];

        $retweet = new Tweet($tweetArray);

        $this->assertEquals('2016-01-20', $retweet->getCreatedAt()->format('Y-m-d'));
        $this->assertEquals(133, $retweet->getTweetId());
        $this->assertTrue($retweet->isRetweeted());

        $this->assertEquals('Test User', $retweet->getUsername());
        $this->assertEquals(555, $retweet->getTwitterUserId());
        $this->assertEquals(100, $retweet->getFriendsCount());
        $this->assertEquals(222, $retweet->getFollowersCount());
        $this->assertEquals(333, $retweet->getFavouritesCount());
    }
}