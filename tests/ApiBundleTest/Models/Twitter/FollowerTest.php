<?php
namespace ApiBundleTest\Models;

use ApiBundle\Models\Twitter\Follower;

class FollowerTest extends \PHPUnit_Framework_TestCase
{
    public function testFollowerModel()
    {
        $user = [
            'created_at' => '2016-01-20',
            'id' => 133,
            'screen_name' => 'Test Name',
            'name' => 'Test User',
        ];

        $follower = new Follower($user);

        $this->assertEquals('2016-01-20', $follower->getCreatedAt()->format('Y-m-d'));
        $this->assertEquals(133, $follower->getUserId());
        $this->assertEquals('Test Name', $follower->getScreenName());
        $this->assertEquals('Test User', $follower->getName());
    }
}