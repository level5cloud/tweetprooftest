<?php
namespace ApiBundleTest\Models;

use ApiBundle\Models\Chart;
use ApiBundle\Models\Models;
use ApiBundle\Models\Twitter\Tweet;

class ChartTest extends \PHPUnit_Framework_TestCase
{
    public function testNoModelsReturnsEmptyChart()
    {
        $models = new Models();

        $chartGenerator = new Chart(
            $models,
            new \DateTimeImmutable('2017-01-10'),
            new \DateTimeImmutable('2017-01-22')
        );

        $chart = $chartGenerator->getChart();

        $this->assertEmpty($chart['data']);
        $this->assertEquals('2017-01-10', $chart['report']['start_date']);
        $this->assertEquals('2017-01-22', $chart['report']['end_date']);
    }

    public function testAddModelsWithinDatesReturnsChart()
    {
        $models = new Models();

        $tweet = new Tweet([ 'created_at' => '2017-01-11' ]);
        $models->addRecord($tweet);
        $tweet = new Tweet([ 'created_at' => '2017-01-11' ]);
        $models->addRecord($tweet);

        $tweet = new Tweet([ 'created_at' => '2017-01-12' ]);
        $models->addRecord($tweet);

        $tweet = new Tweet([ 'created_at' => '2017-01-13' ]);
        $models->addRecord($tweet);
        $tweet = new Tweet([ 'created_at' => '2017-01-13' ]);
        $models->addRecord($tweet);
        $tweet = new Tweet([ 'created_at' => '2017-01-13' ]);
        $models->addRecord($tweet);

        $chartGenerator = new Chart(
            $models,
            new \DateTimeImmutable('2017-01-10'),
            new \DateTimeImmutable('2017-01-15')
        );

        $chart = $chartGenerator->getChart();

        $this->assertNotEmpty($chart['data']);
        $this->assertEquals(2, $chart['data']['2017-01-11']);
        $this->assertEquals(1, $chart['data']['2017-01-12']);
        $this->assertEquals(3, $chart['data']['2017-01-13']);
    }
}
