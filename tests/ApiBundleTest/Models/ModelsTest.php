<?php
namespace ApiBundleTest\Models;

use ApiBundle\Models\Models;
use ApiBundle\Models\Twitter\Tweet;

class ModelsTest extends \PHPUnit_Framework_TestCase
{
    public function testAddTwitterModelReturnsModel()
    {
        $twitterModel = new Tweet([]);

        $models = new Models();
        $models->addRecord($twitterModel);

        $this->assertCount(1, $models->getAll());
    }

    public function testAddMultipleTwitterModelsReturnsModels()
    {
        $models = new Models();

        for ($i = 1; $i <= 10; $i++) {
            $tweetModel = new Tweet([]);
            $models->addRecord($tweetModel);
        }

        $this->assertCount(10, $models->getAll());
    }
}
