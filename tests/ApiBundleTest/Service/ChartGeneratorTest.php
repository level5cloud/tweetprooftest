<?php
namespace ApiBundleTest\Service;

use ApiBundle\Models\Models;
use ApiBundle\Models\Twitter\Tweet;
use ApiBundle\Service\ChartGenerator;

class ChartGeneratorTest extends \PHPUnit_Framework_TestCase
{
    public function testChartGenerator()
    {
        $models = new Models();

        $tweet = new Tweet([ 'created_at' => '2017-01-11' ]);
        $models->addRecord($tweet);
        $tweet = new Tweet([ 'created_at' => '2017-01-11' ]);
        $models->addRecord($tweet);

        $tweet = new Tweet([ 'created_at' => '2017-01-12' ]);
        $models->addRecord($tweet);

        $tweet = new Tweet([ 'created_at' => '2017-01-13' ]);
        $models->addRecord($tweet);
        $tweet = new Tweet([ 'created_at' => '2017-01-13' ]);
        $models->addRecord($tweet);
        $tweet = new Tweet([ 'created_at' => '2017-01-13' ]);
        $models->addRecord($tweet);

        $chartGenerator = new ChartGenerator();
        $chart = $chartGenerator->getChart(
            $models,
            '2017-01-10',
            '2017-01-15'
        );

        $this->assertNotEmpty($chart['data']);
        $this->assertEquals(2, $chart['data']['2017-01-11']);
        $this->assertEquals(1, $chart['data']['2017-01-12']);
        $this->assertEquals(3, $chart['data']['2017-01-13']);
    }

    public function testChartGeneratorDiffDateRange()
    {
        $models = new Models();

        $tweet = new Tweet([ 'created_at' => '2017-01-10' ]);
        $models->addRecord($tweet);
        $tweet = new Tweet([ 'created_at' => '2017-01-10' ]);
        $models->addRecord($tweet);

        $tweet = new Tweet([ 'created_at' => '2017-01-12' ]);
        $models->addRecord($tweet);

        $tweet = new Tweet([ 'created_at' => '2017-01-13' ]);
        $models->addRecord($tweet);
        $tweet = new Tweet([ 'created_at' => '2017-01-13' ]);
        $models->addRecord($tweet);
        $tweet = new Tweet([ 'created_at' => '2017-01-13' ]);
        $models->addRecord($tweet);

        $chartGenerator = new ChartGenerator();
        $chart = $chartGenerator->getChart(
            $models,
            '2017-01-12',
            '2017-01-15'
        );

        $this->assertNotEmpty($chart['data']);
        $this->assertEquals(1, $chart['data']['2017-01-12']);
        $this->assertEquals(3, $chart['data']['2017-01-13']);
    }
}
