<?php

namespace ApiBundle\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use ApiBundle\Service\Twitter\TwitterService;
use Symfony\Component\HttpFoundation\Request;

/**
 * Controller is a Service.  Removed unnecessary abstraction.
 *
 * Class TwitterController
 * @package ApiBundle\Controller
 */
class TwitterController
{
    /**
     * @var TwitterService
     */
    private $twitterService;

    /**
     * TwitterController constructor.
     *
     * @param TwitterService $twitterService
     */
    public function __construct(TwitterService $twitterService)
    {
        $this->twitterService = $twitterService;
    }

    /**
     * @return JsonResponse
     */
    public function tweetsAction(Request $request)
    {
        $chart = $this->twitterService->getTweetChart(
            $request->get('username', 'iisuperwomanii'),
            $request->get('start'),
            $request->get('end')
        );

        return new JsonResponse( [ 'tweets' => $chart ]);
    }

    /**
     * @return JsonResponse
     */
    public function retweetsAction(Request $request)
    {
        $chart = $this->twitterService->getRetweetChart(
            $request->get('username', 'iisuperwomanii'),
            $request->get('start'),
            $request->get('end')
        );

        return new JsonResponse( [ 'retweets' => $chart ]);
    }

    /**
     * @return JsonResponse
     */
    public function followersAction(Request $request)
    {
        $chart = $this->twitterService->getFollowersChart(
            $request->get('username', 'iisuperwomanii'),
            $request->get('start'),
            $request->get('end')
        );

        return new JsonResponse( [ 'followers' => $chart ]);
    }
}
