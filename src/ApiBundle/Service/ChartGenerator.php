<?php
namespace ApiBundle\Service;

use ApiBundle\Models\Models;
use ApiBundle\Models\Chart;

/**
 * Class ChartGenerator
 * @package ApiBundle\Service
 */
class ChartGenerator
{
    /**
     * @param Models $models
     * @param \DateTimeImmutable $start
     * @param \DateTimeImmutable $end
     *
     * @return array
     */
    public function getChart(
        Models $models,
        $start = null,
        $end = null
    )
    {
        $chartGenerator = new Chart(
            $models,
            $this->getValidStartDate($start),
            $this->getValidEndDate($end)
        );

        return $chartGenerator->getChart();
    }

    /**
     * @param null $date
     *
     * @return \DateTimeImmutable
     */
    private function getValidStartDate($date = null)
    {
        if ($date === null || ! $this->validateDate($date)) {
            return new \DateTimeImmutable('-7 days');
        }

        return new \DateTimeImmutable($date);
    }

    /**
     * @param null $date
     *
     * @return \DateTimeImmutable
     */
    private function getValidEndDate($date = null)
    {
        if ($date === null || ! $this->validateDate($date)) {
            return new \DateTimeImmutable('now');
        }

        return new \DateTimeImmutable($date);
    }

    /**
     * @param null $date
     *
     * @return bool
     */
    private function validateDate($date = null)
    {
        if ($date === null) {
            return false;
        }

        $dateObject = \DateTimeImmutable::createFromFormat('Y-m-d', $date);
        return ($dateObject && $dateObject->format('Y-m-d') === $date);
    }
}