<?php
namespace ApiBundle\Service\Twitter;

use ApiBundle\Models\Models;
use ApiBundle\Models\Twitter\Tweet;
use ApiBundle\Client\ClientInterface;
use ApiBundle\Service\ChartGenerator;
use ApiBundle\Models\Twitter\Follower;

/**
 * Class TwitterService
 * @package ApiBundle\Service\Twitter
 */
class TwitterService
{
    /**
     * @var $client ClientInterface
     */
    private $client;

    /**
     * @var ChartGenerator
     */
    private $chartGenerator;

    /**
     * TwitterService constructor.
     *
     * @param ClientInterface $client
     */
    public function __construct(ClientInterface $client)
    {
        $this->client = $client;
        $this->chartGenerator = new ChartGenerator();
    }

    /**
     * @param $screenName
     * @param int $count
     *
     * @return array
     */
    private function getRetweets($screenName, $count = 15)
    {
        return $this->client->get(
            'search/tweets',
            [
                "q"     => 'from:@' . $screenName,
                "count" => $count,
            ]
        );
    }

    /**
     * @param $screenName
     * @param array $count
     * @param bool|true $excludeReplies
     *
     * @return array
     */
    private function getTweets($screenName, $count = 100, $excludeReplies = true)
    {
        return $this->client->get(
            'statuses/user_timeline',
            [
                "screen_name"     => $screenName,
                "count"           => $count,
                "exclude_replies" => $excludeReplies,
            ]
        );
    }
    /**
     * @param $screenName
     * @param array $count
     * @param bool|true $excludeReplies
     *
     * @return array
     */
    private function getFollowers($screenName)
    {
        return $this->client->get(
            'followers/list',
            [
                "screen_name" => $screenName,
            ]
        );
    }

    /**
     * @param string $screenName
     * @param null $start
     * @param null $end
     *
     * @return array
     */
    public function getFollowersChart($screenName, $start = null, $end = null)
    {
        // Get tweets from API
        $followers = (array)$this->getFollowers($screenName);

        $models = new Models();

        foreach ($followers['users'] as $tweet) {
            $model = new Follower((array) $tweet);
            $models->addRecord($model);
        }

        return $this->chartGenerator->getChart($models, $start, $end);
    }

    /**
     * @param string $screenName
     * @param null $start
     * @param null $end
     *
     * @return array
     */
    public function getRetweetChart($screenName, $start = null, $end = null)
    {
        // Get tweets from API
        $retweets = (array)$this->getRetweets($screenName, 100);

        $models = new Models();

        foreach ($retweets['statuses'] as $tweet) {
            $model = new Tweet((array) $tweet);

            if ($model->isRetweeted()) {
                $models->addRecord($model);
            }
        }

        return $this->chartGenerator->getChart($models, $start, $end);
    }

    /**
     * @param string $screenName
     * @param null $start
     * @param null $end
     *
     * @return array
     */
    public function getTweetChart($screenName, $start = null, $end = null)
    {
        // Get tweets from API
        $tweets = (array)$this->getTweets($screenName, 100, true);

        $models = new Models();

        foreach ($tweets as $tweet) {
            $model = new Tweet((array) $tweet);

            if (!$model->isRetweeted()) {
                $models->addRecord($model);
            }
        }

        return $this->chartGenerator->getChart($models, $start, $end);
    }
}