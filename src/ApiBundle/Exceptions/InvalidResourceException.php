<?php
namespace ApiBundle\Exceptions;

/**
 * Class InvalidResourceException
 */
class InvalidResourceException extends \Exception
{
    public function __construct($message = '')
    {
        return new \Exception('Invalid Resource API Error: ' . $message);
    }
}