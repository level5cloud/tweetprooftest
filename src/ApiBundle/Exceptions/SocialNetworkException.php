<?php
namespace ApiBundle\Exceptions;

/**
 * Class SocialNetworkException
 */
class SocialNetworkException extends \Exception
{
    /**
     * SocialNetworkException constructor.
     *
     * @param string $resource
     * @param string $message
     */
    public function __construct($resource = null, $message = null)
    {
        return parent::__construct('Social Network [' . $resource . '] error: ' . $message);
    }
}