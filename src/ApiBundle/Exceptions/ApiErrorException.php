<?php
namespace ApiBundle\Exceptions;

/**
 * Class ApiErrorException
 */
class ApiErrorException extends \Exception
{
    public function __construct($message = '')
    {
        return new \Exception('API Error: ' . $message);
    }
}