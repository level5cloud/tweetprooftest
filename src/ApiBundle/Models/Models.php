<?php
namespace ApiBundle\Models;

/**
 * Encapsulation, Models service
 *
 * Class Models
 * @package ApiBundle\Models
 */
class Models
{
    /**
     * @var array
     */
    private $records = [];

    /**
     * @param ModelInterface $model
     *
     * @return $this
     */
    public function addRecord(ModelInterface $model)
    {
        $this->records[] = $model;
        return $this;
    }

    /**
     * @return array
     */
    public function getAll()
    {
        return $this->records;
    }
}