<?php
namespace ApiBundle\Models;

use DateTimeImmutable;

/**
 * Class ModelInterface
 */
interface ModelInterface
{
    /**
     * @return DateTimeImmutable
     */
    public function getCreatedAt();
}