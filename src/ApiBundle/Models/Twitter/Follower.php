<?php
namespace ApiBundle\Models\Twitter;

use Doctrine\Common\Collections\ArrayCollection;
use ApiBundle\Models\ModelInterface;
use DateTimeImmutable;

/**
 * @note Immutable & Encapsulation model
 *
 * Class Follower
 * @package ApiBundle\Models\Twitter
 */
class Follower implements ModelInterface
{
    /**
     * @var DateTimeImmutable;
     */
    private $createdAt;

    /**
     * @var int
     */
    private $userId;

    /**
     * @var string
     */
    private $screenName;

    /**
     * @var string
     */
    private $name;

    /**
     * Follower constructor.
     *
     * @param array $apiResponse
     */
    public function __construct(array $apiResponse)
    {
        $user = new ArrayCollection($apiResponse);

        $this->createdAt = new \DateTimeImmutable($user->get('created_at'));
        $this->userId = $user->get('id');
        $this->screenName = $user->get('screen_name');
        $this->name = $user->get('name');

        return $this;
    }

    /**
     * @return DateTimeImmutable
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @return int
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * @return string
     */
    public function getScreenName()
    {
        return $this->screenName;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
}