<?php
namespace ApiBundle\Models\Twitter;

use Doctrine\Common\Collections\ArrayCollection;
use ApiBundle\Models\ModelInterface;
use DateTimeImmutable;

/**
 * @note Immutable & Encapsulation model
 * Tweets and Retweets
 *
 * Class Tweet
 * @package ApiBundle\Models\Twitter
 */
class Tweet implements ModelInterface
{
    /**
     * @var DateTimeImmutable;
     */
    private $createdAt;

    /**
     * @var int
     */
    private $tweetId;

    /**
     * @var bool
     */
    private $retweeted;

    /**
     * @var string
     */
    private $username;

    /**
     * @var int
     */
    private $twitterUserId;

    /**
     * @var int
     */
    private $friendsCount;

    /**
     * @var int
     */
    private $followersCount;

    /**
     * @var int
     */
    private $favouritesCount;

    /**
     * Twitter constructor.
     *
     * @param array $apiResponse
     */
    public function __construct(array $apiResponse)
    {
        $twitterRecordArray = (array) $apiResponse;
        $tweet = new ArrayCollection($twitterRecordArray);

        $twitterUser = (array) $tweet->get('user');

        $user = new ArrayCollection($twitterUser);

        $this->createdAt = new \DateTimeImmutable($tweet->get('created_at'));
        $this->tweetId = $tweet->get('id');
        $this->retweeted = $tweet->containsKey('retweeted_status');
        $this->username = $user->get('name');

        $this->twitterUserId = $user->get('id');
        $this->friendsCount = $user->get('friends_count');
        $this->followersCount = $user->get('followers_count');
        $this->favouritesCount = $user->get('favourites_count');

        return $this;
    }

    /**
     * @return DateTimeImmutable
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @return int
     */
    public function getTweetId()
    {
        return $this->tweetId;
    }

    /**
     * @return boolean
     */
    public function isRetweeted()
    {
        return $this->retweeted;
    }

    /**
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @return int
     */
    public function getTwitterUserId()
    {
        return $this->twitterUserId;
    }

    /**
     * @return int
     */
    public function getFriendsCount()
    {
        return $this->friendsCount;
    }

    /**
     * @return int
     */
    public function getFollowersCount()
    {
        return $this->followersCount;
    }

    /**
     * @return int
     */
    public function getFavouritesCount()
    {
        return $this->favouritesCount;
    }
}