<?php
namespace ApiBundle\Models;

/**
 * Class Chart
 * @package ApiBundle\Models\Twitter
 */
class Chart
{
    /**
     * @var Models
     */
    private $models;

    /**
     * @var \DateTimeImmutable
     */
    private $start;

    /**
     * @var \DateTimeImmutable
     */
    private $end;

    /**
     * RetweetChart constructor.
     *
     * @param Models $models
     * @param \DateTimeImmutable $start
     * @param \DateTimeImmutable $end
     */
    public function __construct(
        Models $models,
        \DateTimeImmutable $start,
        \DateTimeImmutable $end
    ) {
        $this->models = $models;
        $this->start = $start;
        $this->end = $end;
    }

    /**
     * @return array
     */
    public function getChart()
    {
        $data = [];

        /** @var ModelInterface $model */
        foreach ($this->models->getAll() as $model) {
            if (! $this->isBetweenSpecifiedDates($model->getCreatedAt())) {
                continue;
            }

            $data = $this->updateCount(
                $data,
                $model->getCreatedAt()->format('Y-m-d')
            );
        }

        $chart['data'] = $data;
        $chart['report']['start_date'] = $this->start->format('Y-m-d');
        $chart['report']['end_date'] = $this->end->format('Y-m-d');

        return $chart;
    }

    /**
     * @param array $chart
     * @param string $date
     *
     * @return array
     */
    private function updateCount(array $data, $date)
    {
        if (! isset($data[$date])) {
            $data[$date] = 1;
            return $data;
        }

        $data[$date]++;

        return $data;
    }

    /**
     * @param \DateTimeImmutable $date
     *
     * @return bool
     */
    private function isBetweenSpecifiedDates(\DateTimeImmutable $date)
    {
        return ($date->getTimestamp() >= $this->start->getTimestamp())
            && ($date->getTimestamp() <= $this->end->getTimestamp());
    }
}