<?php
namespace ApiBundle\Client;

use Abraham\TwitterOAuth\TwitterOAuth;
use ApiBundle\Exceptions\SocialNetworkException;

/**
 * @note ServiceFactory
 *
 * Class TwitterClient
 * @package ApiBundle\Client
 */
class TwitterClient implements ClientInterface
{
    /**
     * @var TwitterOAuth
     */
    private $twitterClient;

    /**
     * TwitterClient constructor.
     *
     * @param TwitterOAuth $twitterClient
     */
    public function __construct(TwitterOAuth $twitterClient)
    {
        $this->twitterClient = $twitterClient;
    }

    /**
     * @param string $url
     * @param array $parameters
     *
     * @return array
     * @throws SocialNetworkException
     */
    public function get(string $url, array $parameters)
    {
        $apiResponse = (array) $this->twitterClient->get($url, $parameters);

        // check for errors from API
        $this->twitterResponseError($apiResponse);

        return $apiResponse;
    }

    /**
     * @param array $apiResponse
     *
     * @throws SocialNetworkException
     */
    private function twitterResponseError(array $apiResponse)
    {
        if (isset($apiResponse['errors'])) {
            throw new SocialNetworkException(
                'twitter',
                json_encode($apiResponse['errors'])
            );
        }
    }
}