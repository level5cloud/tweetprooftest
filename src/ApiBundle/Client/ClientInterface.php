<?php
namespace ApiBundle\Client;

use ApiBundle\Exceptions\SocialNetworkException;

/**
 * Interface ClientInterface
 * @package ApiBundle\Client
 */
interface ClientInterface
{
    /**
     * @param string $url
     * @param array $parameters
     *
     * @return array
     * @throws SocialNetworkException
     */
    public function get(string $url, array $parameters);
}