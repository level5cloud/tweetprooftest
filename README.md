TweetProof Analytics
===============

This Symfony app can handle analytics on a specific user' followers, tweets and retweet counts.
---------------

@note
The default date range is 1 week from the current date.

- Default timeline is iisuperwomanii.
- Parameters are: `username`, `start`, and `end`.
- Date values (`start` and `end`) should be in the format: `YYYY-MM-DD`.  Timestamps are not reliable.

Improvements
---------------

- An additional call can be made to twitter to check if a username exists.  However, this is costly as there are limits on connection.
- When a specific user wants to check their own timeline, we can crawl the data locally to enhance analytics.
- Cache calls; another way to reduce the amount of calls is to cache data in the past X minutes.  Identical calls can have cached results (TTL).
- If there is a chance to change framework, then create a TwitterClientFactory and inject the Services rather than use Symfony services.yml.
- Integration tests with Behat + Mink

Instructions
---------------
Run command: 'php -S localhost:8004' and load http://localhost:8004 in browser.

Run `composer install` (install composer globally)

Run tests with `./vendor/bin/phpunit`

http://localhost:8004/web/app.php/api/twitter/tweets (tweets)

http://localhost:8004/web/app.php/api/twitter/retweets (retweets)

http://localhost:8004/web/app.php/api/twitter/followers (followers)


*Requirements: PHP 7+ ONLY!  Will not work due to declarations and type hints on PHP <= 5.6.*

Terms
---------------
The code is only for reviewing purposes.  Any use of this code will be seen as plagiarism, resulting in copyright infringement.  Written permission must be required.


&copy; Copyright 22 January 2017, Gaurav Malhotra (gaurav@level5websites.com / gaurav_malhotra2@yahoo.co.uk)